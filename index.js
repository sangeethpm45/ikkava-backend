const express = require("express");
const app = express();

const dataservice = require("./services/dataservice");
const bcrypt = require("bcrypt");
const FileUpload = require("express-fileupload");
const {
  toLatLon,
  insideCircle,
  toLatitudeLongitude,
  headingDistanceTo,
  moveTo,
  insidePolygon,
} = require("geolocation-utils");
const { json } = require("express");
app.use(express.json());
app.use(FileUpload());
const Auth = (req, res, next) => {
  token = req.header("x-auth-token");
  if (token) {
    next();
  } else {
    return res.json({
      status: false,
      statusCode: 403,
      message: "forbidden",
    });
  }
};

app.post("/api/location", (req, res) => {
  const center = { lat: 51, lon: 4 };
  const radius = 20000;
  lat = parseInt(req.body.lat);
  lon = parseInt(req.body.lon);
  console.log(headingDistanceTo(center,{ lat: lat, lon: lon })) 
  const result = insideCircle({ lat: lat, lon: lon }, center, radius);

  if (result) {
    res.json({
      status: true,
      statusCode: 200,
      message: "delivery available",
    });
  } else {
    res.json({
      status: false,
      statusCode: 403,
      message: "Not available",
    });
  }
});
app.post("/api/user/register", async (req, res) => {
  salt = await bcrypt.genSalt(10);
  hashedpassword = await bcrypt.hash(req.body.password, salt);
  dataservice
    .register(
      req.body.fname,
      req.body.email,
      req.body.phone,
      hashedpassword,
      req.body.scope
    )
    .then((result) => {
      res.status(result.statusCode).json(result);
    });
});
app.post("/api/user/login", (req, res) => {
  dataservice.Login(req.body.email, req.body.password).then((result) => {
    res.status(result.statusCode).json(result);
  });
});

app.post("/api/user/fileupload", (req, res) => {
  if (!req.files) {
    res.json({ status: false, statusCode: 422, message: "file not found" });
  } else {
    let avatar = req.files.avatar;
    avatar.mv("./files" + avatar.name);
    res.json({
      status: true,
      message: "File is uploaded",
      data: {
        name: avatar.name,
        mimetype: avatar.mimetype,
        size: avatar.size,
      },
    });
  }
});

app.get("/api/users/:id", Auth, (req, res) => {
  dataservice.users_details(req.params.id).then((result) => {
    res.status(result.statusCode).json(result);
  });
});

app.get("/api/user", (req, res) => {
  dataservice.UserbyId(req.query.email).then((result) => {
    res.status(result.statusCode).json(result);
  });
});

app.delete("/api/user/delete/:id", (req, res) => {
  dataservice.deleteUser(req.params.id).then((result) => {
    res.status(result.statusCode).json(result);
  });
});

app.patch("/api/user/update/:id", (req, res) => {
  dataservice.UpdateDetails(req.params.id, req.body.email).then((result) => {
    res.status(result.statusCode).json(result);
  });
});

app.listen(3000, () => {
  console.log("listening on 3000");
});
