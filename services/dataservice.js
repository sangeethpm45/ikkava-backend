const db = require("./db");
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");

const register = async (fname, email, phone, password, scope) => {
  const result = await db.User.findOne({ Email: email });
  if (result) {
    return {
      status: false,
      statusCode: 409,
      message: "User already exists",
    };
  } else {
    const newUser = new db.User({
      FullName: fname,
      Email: email,
      Phone: phone,
      Password: password,
      Scope: scope,
    });

    try {
      const result_1 = await newUser.save();
      return {
        status: true,
        statusCode: 200,
        message: "Sucess",
      };
    } catch (ex) {
      return {
        status: false,
        statusCode: 422,
        message: ex.message,
      };
    }
  }
};

const Login = async (email, password) => {
  const result = await db.User.findOne({ Email: email });

  Verifypassword = await bcrypt.compare(password, result.Password);

  if (result) {
    if (Verifypassword) {
      const data = { name: result.FullName, phone: result.Phone };
      const token=jwt.sign(data,'secretkey')

      console.log(Verifypassword);
      return {
        status: true,
        statusCode: 200,
        message: "Login sucess",
        token:token
      };
    } else {
      return {
        status: false,
        statusCode: 422,
        message: "Invalid Credentials",
      };
    }
  }
  return {
    status: false,
    statusCode: 422,
    message: "Invalid Credentials",
  };
};

const users_details = async (id) => {
  result = await db.User.findOne({ Email: id });
  if (result.Scope == "admin") {
    user = await db.User.find();

   
    if (user) {
      return {
        status: true,
        statusCode: 200,
        users: user,
      };
    }
  } else {
    return {
      status: false,
      statusCode: 403,
      message: "Unauthorised Access",
    };
  }
};

const UserbyId = async (id) => {
  const result = await db.User.find({ Email: id });
  if (result) {
    console.log(result);
    return {
      status: true,
      statusCode: 200,
      user: result,
    };
  }
  return {
    status: false,
    statusCode: 422,
    message: "No User Found",
  };
};

const deleteUser = async (id) => {
  result = await db.User.deleteOne({ Email: id });
  if (result) {
    return {
      status: true,
      statusCode: 200,
      message: "Deleted Succesffully",
    };
  } else {
    return {
      status: true,
      statusCode: 200,
      message: "User Not Found",
    };
  }
};

const UpdateDetails = async (id, email) => {
  result = await db.User.updateOne({ Email: id }, { $set: { Email: email } });
  if (result) {
    return {
      status: true,
      statusCode: 200,
      message: "updated succesfully",
    };
  } else {
    return {
      status: false,
      statusCode: 422,
      message: "failed",
    };
  }
};
const Fileupload=async ()=>{

}
module.exports = {
  register,
  Login,
  users_details,
  UserbyId,
  deleteUser,
  UpdateDetails,
};
