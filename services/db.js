const mongoose =require('mongoose')
mongoose.connect('mongodb://localhost:27017/ikkava_server', {useNewUrlParser:true,useUnifiedTopology:true})
const User=mongoose.model('User',{
    FullName:
    {
    type:String,
    required:true,
    minLength:3,
    maxLenght:255
    },
    Email:{type:String,required:true},
    Phone:{type:Number,required:true},
    Password:{type:String,required:true},
    Scope:{type:String,required:true}
})
module.exports={
    User
}